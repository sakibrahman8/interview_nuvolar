provider "google" {
  credentials = file("path-to-your-credentials-file.json")
  project     = var.project_id
  region      = var.region
}

module "vpc" {
  source      = "./modules/vpc"
  project_id  = var.project_id
  vpc_name    = var.vpc_name
  region      = var.region
  subnet_cidr = var.subnet_cidr
}

module "gke" {
  source      = "./modules/gke"
  project_id  = var.project_id
  region      = var.region
  vpc_name    = module.vpc.vpc_name
  subnet_name = module.vpc.subnet_name
  subnet_cidr = module.vpc.subnet_cidr
  gke_cluster_name = var.gke_cluster_name
  node_count  = var.node_count
  preemptible = var.preemptible
  machine_type = var.machine_type
  disk_size_gb = var.disk_size_gb
  min_node_count = var.min_node_count
  max_node_count = var.max_node_count
}

module "nat" {
  source      = "./modules/nat"
  network_id  = module.vpc.network_id
  region      = var.region
  nat_router_name  = var.nat_router_name
  nat_gateway_name = var.nat_gateway_name
}

module "artifact_registry" {
  source        = "./modules/artifact_registry"
  location      = var.region
  repository_id = var.artifact_registry_repository_id
}
