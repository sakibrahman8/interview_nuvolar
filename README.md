### Project Overview

This project involves deploying a microservices-based application on Google Kubernetes Engine (GKE). The application comprises three main components:

1. **API Gateway**: Serves as the entry point for the application, routing requests to the appropriate microservices.
2. **Order Service**: Manages order-related operations.
3. **Customer Service**: Handles customer-related information and operations.

The deployment is managed using Terraform and Helm, with an emphasis on security and scalability.

### Prerequisites

1. **Google Cloud Platform Account**: Ensure you have a GCP account with billing enabled.
2. **Google Cloud SDK**: Install the [Google Cloud SDK](https://cloud.google.com/sdk/docs/install) on your machine. This includes the `gcloud` command-line tool.
3. **Terraform**: Install [Terraform](https://www.terraform.io/downloads.html).
4. **Helm**: Install [Helm](https://helm.sh/docs/intro/install/), the package manager for Kubernetes.
5. **Kubectl**: Ensure `kubectl` is installed, which is usually included in the Google Cloud SDK installation.

### Step-by-Step Guide

#### Step 1: Create a Service Account

1. In the Google Cloud Console, go to "IAM & Admin" > "Service Accounts".
2. Click "Create Service Account" and fill in the necessary details.
3. Assign the required roles (like "Kubernetes Engine Admin", "Compute Admin", etc.).
4. Create a JSON key for this service account and download it. This will be your credentials file.

#### Step 2: Reserve a Global Static IP Address

1. In the Google Cloud Console, go to "VPC Network" > "External IP addresses".
2. Click "Reserve Static Address".
3. Choose "Global" for the type and give it a name.
4. Note down the reserved IP address (add an A record on your DNS)

#### Step 3: Update Terraform and Helm Configurations

1. **Terraform**: 
   - Update your Terraform files to replace the placeholders for the project ID, bucket name, and credentials file path with your actual values.
   - Ensure your Terraform configurations define the necessary resources like GKE cluster, VPC, etc.

2. **Helm**:
   - Update the `values.yaml` file in each of your Helm charts (`api-gateway`, `customer-service`, `order-service`) to reflect the correct image paths, domain names, and any other specific configurations.
   - Specifically, update the `api-gateway/values.yaml` file to use the reserved static IP address.

You can also run these command to do replaces for you:

```
find $current_dir -type f -exec sed -i "s/YOUR_PROJECT_ID/$project_id/g" {} +
find $current_dir -type f -exec sed -i "s/YOUR_BUCKET_NAME/$bucket_name/g" {} +
find $current_dir -type f -exec sed -i "s|path-to-your-credentials-file.json|$credentials_path|g" {} +
find $current_dir -type f -exec sed -i "s/yourdomain.com/$domain_name/g" {} +
find $current_dir -type f -exec sed -i "s/YOUR_STATIC_IP/$ip_address/g" {} +
```
**Replace the variables with actual values e.g. $credentials_path with sa.json**

#### Step 4: Deploy Infrastructure with Terraform

1. Navigate to your Terraform files' directory.
2. Initialize Terraform:
   ```bash
   terraform init
   ```
3. Create a Terraform plan:
   ```bash
   terraform plan -out=tfplan
   ```
4. Apply the Terraform plan to deploy your infrastructure:
   ```bash
   terraform apply "tfplan"
   ```

#### Step 5: Get Credentials for Your GKE Cluster

1. Use the `gcloud` command to fetch credentials for your GKE cluster:
   ```bash
   gcloud container clusters get-credentials YOUR_CLUSTER_NAME --region YOUR_CLUSTER_REGION --project YOUR_PROJECT_ID --internal-ip
   ```

#### Step 6: Deploy Services with Helm

1. Navigate to the directory of each Helm chart.
2. Install each service:
   ```bash
   helm install customer-service ./customer-service -f ./customer-service/values.yaml
   helm install order-service ./order-service -f ./order-service/values.yaml
   helm install api-gateway ./api-gateway -f ./api-gateway/values.yaml
   ```

#### Step 7: Verify Deployment

1. Check the status of your deployed services and pods:
   ```bash
   kubectl get services
   kubectl get pods
   ```
2. Ensure that the `api-gateway` service is correctly exposed and that the ingress is configured with the Google-managed SSL certificate.

### Notes

- **run_all.sh**: You may have noticed a script called run_all.sh, you can run this but ensure that all pre-requisites are met.
- **Terraform State**: Be aware that Terraform maintains state files for your deployments. Handle these files securely and consider using a remote backend for state management.
- **Google Cloud Quotas**: Check your quotas in GCP to ensure they are sufficient for the resources being deployed.
- **Security Practices**: Follow security best practices for managing service account keys and other sensitive information.

### Tools and Technologies

1. **Google Kubernetes Engine (GKE)**: Chosen for its seamless integration with other Google Cloud services, robust scalability, and ease of managing Kubernetes clusters. GKE provides a managed environment for deploying, managing, and scaling containerized applications using Google infrastructure.

2. **Terraform**: Utilized for infrastructure as code (IaC) to automate the creation and management of cloud resources. Terraform is known for its declarative code and ability to manage a wide range of cloud services. It's used here to set up the GKE cluster, networking resources, and other necessary infrastructure components.

3. **Helm**: Helm is a package manager for Kubernetes, simplifying the deployment and management of applications on Kubernetes. Helm charts are used to define, install, and upgrade Kubernetes applications. In this project, Helm charts are used for deploying the microservices with a focus on configurability and version management.

4. **Google-managed SSL Certificates**: Chosen for their ease of use and seamless integration with GKE and Google Cloud Load Balancers. These certificates provide automated SSL/TLS certificate management, improving the security of the application.

5. **Ingress Controller (GCE Ingress for GKE)**: Used to manage external access to the services in the cluster, typically HTTP/HTTPS traffic. Ingress may provide load balancing, SSL termination, and name-based virtual hosting.

### Architecture and Security

- The application is containerized and orchestrated using Kubernetes, allowing for scalable and resilient deployment.
- Network communication is secured through SSL/TLS, leveraging Google-managed SSL Certificates for simplicity and reliability.
- The API Gateway pattern is used to provide a single entry point for the application, simplifying the API structure and providing a layer for potential security enhancements like rate limiting or authentication.
- Terraform's infrastructure as code approach ensures repeatable, consistent deployment setups and eases the process of infrastructure management and versioning.
- Helm charts provide templated deployments, allowing for configurable and repeatable application setups.

### Conclusion

This project demonstrates a cloud-native approach to deploying a microservices-based application, leveraging the strengths of GKE, Terraform, and Helm for a robust, scalable, and secure deployment. The use of Google-managed SSL certificates and GCE Ingress ensures secure and efficient traffic management for the services. 

The architecture is designed with scalability, resilience, and security in mind, aligning with best practices for cloud-native application development and deployment.