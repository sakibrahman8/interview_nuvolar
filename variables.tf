variable "project_id" {
  description = "The GCP project ID."
  type        = string
}

variable "region" {
  description = "The region to host the cluster in."
  type        = string
}

variable "vpc_name" {
  description = "The name of the VPC."
  type        = string
}

variable "subnet_cidr" {
  description = "The CIDR block for the VPC subnet."
  type        = string
}

variable "nat_router_name" {
  description = "The name of the NAT router."
  type        = string
}

variable "nat_gateway_name" {
  description = "The name of the NAT gateway."
  type        = string
}

variable "gke_cluster_name" {
  description = "The name of the GKE cluster."
  type        = string
}

variable "node_count" {
  description = "Number of nodes in the GKE cluster."
  type        = number
}

variable "preemptible" {
  description = "Use preemptible VMs for the GKE cluster."
  type        = bool
}

variable "machine_type" {
  description = "Machine type for the GKE cluster nodes."
  type        = string
}

variable "disk_size_gb" {
  description = "Disk size in GB for nodes in the GKE cluster."
  type        = number
  default     = 20
}

variable "min_node_count" {
  description = "Minimum number of nodes for autoscaling."
  type        = number
}

variable "max_node_count" {
  description = "Maximum number of nodes for autoscaling."
  type        = number
}

variable "artifact_registry_repository_id" {
  description = "The ID of the repository."
  type        = string
}
