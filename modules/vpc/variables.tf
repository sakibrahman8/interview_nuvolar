variable "project_id" {
  description = "The GCP project ID."
  type        = string
}

variable "vpc_name" {
  description = "The name of the VPC."
  type        = string
}

variable "region" {
  description = "The region where the VPC will be created."
  type        = string
}

variable "subnet_cidr" {
  description = "CIDR block for the subnet."
  type        = string
}
