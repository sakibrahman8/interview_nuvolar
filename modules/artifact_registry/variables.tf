variable "location" {
  description = "The location for the artifact repository."
  type        = string
}

variable "repository_id" {
  description = "The ID of the repository."
  type        = string
}
