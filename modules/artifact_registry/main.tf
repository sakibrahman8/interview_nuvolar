resource "google_artifact_registry_repository" "docker_proxy" {
  location      = var.location
  repository_id = var.repository_id
  description   = "Docker proxy repository"
  format        = "DOCKER"
  mode          = "REMOTE_REPOSITORY"

  remote_repository_config {
    description = "docker hub"
    docker_repository {
      public_repository = "DOCKER_HUB"
    }
  }
}
