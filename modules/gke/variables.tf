variable "project_id" {
  description = "The GCP project ID."
  type        = string
}

variable "region" {
  description = "The region where the GKE cluster will be created."
  type        = string
}

variable "vpc_name" {
  description = "The name of the VPC where the GKE cluster will be deployed."
  type        = string
}

variable "subnet_name" {
  description = "The name of the subnet where the GKE cluster will be deployed."
  type        = string
}

variable "subnet_cidr" {
  description = "CIDR block of the subnet where GKE master will be authorized."
  type        = string
}

variable "gke_cluster_name" {
  description = "The name of the GKE cluster."
  type        = string
}


variable "node_count" {
  description = "Number of nodes in the GKE cluster."
  type        = number
}

variable "preemptible" {
  description = "Use preemptible VMs for the GKE cluster."
  type        = bool
}

variable "machine_type" {
  description = "Machine type for the GKE cluster nodes."
  type        = string
}

variable "disk_size_gb" {
  description = "Disk size in GB for nodes in the GKE cluster."
  type        = number
  default     = 20
}

variable "min_node_count" {
  description = "Minimum number of nodes for autoscaling."
  type        = number
}

variable "max_node_count" {
  description = "Maximum number of nodes for autoscaling."
  type        = number
}
