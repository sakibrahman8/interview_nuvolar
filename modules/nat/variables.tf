variable "network_id" {
  description = "The ID of the network where the NAT will be deployed."
  type        = string
}

variable "region" {
  description = "The region where the NAT will be deployed."
  type        = string
}

variable "nat_router_name" {
  description = "The name of the NAT router."
  type        = string
}

variable "nat_gateway_name" {
  description = "The name of the NAT gateway."
  type        = string
}
