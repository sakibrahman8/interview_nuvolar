#!/bin/bash

# Prompt for user inputs
read -p "Enter your Google Cloud Project ID: " project_id
read -p "Enter your Google Cloud Bucket Name: " bucket_name
read -p "Enter the path to your Google Cloud credentials file: " credentials_path
read -p "Enter your domain name: " domain_name
read -p "Enter your static ip address: " ip_address

# Get the current directory (directory where the script is run)
current_dir=$(pwd)

# Replace placeholders in Terraform and Helm files in the current directory
find $current_dir -type f -exec sed -i "s/YOUR_PROJECT_ID/$project_id/g" {} +
find $current_dir -type f -exec sed -i "s/YOUR_BUCKET_NAME/$bucket_name/g" {} +
find $current_dir -type f -exec sed -i "s|path-to-your-credentials-file.json|$credentials_path|g" {} +
find $current_dir -type f -exec sed -i "s/yourdomain.com/$domain_name/g" {} +
find $current_dir -type f -exec sed -i "s/YOUR_STATIC_IP/$ip_address/g" {} +

# Run Terraform commands
terraform init
terraform plan -out=tfplan
terraform apply "tfplan"

# Deploy each service with Helm
helm install customer-service ./customer-service -f ./customer-service/values.yaml
helm install order-service ./order-service -f ./order-service/values.yaml
helm install api-gateway ./api-gateway -f ./api-gateway/values.yaml

echo "Deployment completed successfully!"
