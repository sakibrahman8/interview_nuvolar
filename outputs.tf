output "gke_cluster_endpoint" {
  value = module.gke.endpoint
}

output "gke_cluster_ca_certificate" {
  value = module.gke.ca_certificate
}
